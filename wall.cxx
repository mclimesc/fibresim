#include "wall.h"
#include <iostream>
#include <array>
wall::wall(){
	fx1=0;
	fy1=0;
	fx2=1;
	fy2=1;
	mrng = new TRandom3();
}

wall::wall(double x1,double x2,double y1,double y2,char xy,TRandom3 *rng){

	fx1=x1;
	fx2=x2;
	fy1=y1;
	fy2=y2;
	fxy=xy;
	mrng = rng;
}


wall::wall(double x1,double y1,double r,double ratio,bool round){

		fx1=x1;
		fy1=y1;
		fr= r;
	
		if(ratio >0 && ratio < 1) ratio = ratio; 
		else 			  ratio =1;
		IsRound=1;
}

wall::~wall(){
}

bool wall::Disappear(photon *l){
	bool ret=0;
	float rnag = mrng->Rndm();
	if(rnag > freflectivity){
		l->SetIsDisappear();
		ret=1;
	}
	return ret;
}

std::array<double,3> wall::ComputeParameters(double x1,double y1,double z1,double x2,double y2,double z2){
	std::array<double,3> par_arr ={x2-x1,y2-y1,z2-z1};
	return par_arr;
}

double wall::Compute_tparx(double xpar){
	return fx1/xpar;
}

double wall::Compute_tpary(double ypar){
	return fy1/ypar;
}

std::array<double,3> wall::ReturnNewXYZ(double tpar, std::array<double,3> par_arr){
	std::array<double,3> ncoord_arr = {tpar*par_arr[0],tpar*par_arr[1],tpar*par_arr[2]};
	return ncoord_arr;
}

void wall::SetNewLastPoint(photon *l, std::array<double,3> ncoord_arr){
	l->SetNewLastPoint(ncoord_arr[0],ncoord_arr[1],ncoord_arr[2]);
}


void wall::Reflectx(double lastx, double lasty, double lastz,photon *l){
	if((lastx >=0 && fx1 >= 0 )|| (lastx < 0 && fx1 < 0))l->SetNextPoint(2*fx1-lastx,lasty,lastz);	
//	else if((lastx < 0 && fx1 > 0) || (lastx > 0 && fx1 < 0)){ l->SetNextPoint(fx1-lastx,lasty,lastz);	std::cout << "fx1 " << fx1 << " lastx " << lastx <<std::endl;}
}

void wall::Reflecty(double lastx, double lasty, double lastz,photon *l){
	if((lasty >0 && fy1 > 0 )|| (lasty < 0 && fy1 < 0))l->SetNextPoint(lastx,2*fy1-lasty,lastz);	
//	else if((lasty < 0 && fy1 > 0) || (lasty > 0 && fy1 < 0)) l->SetNextPoint(lastx,lasty+2*fy1,lastz);	
}
bool wall::IsIntercept(photon *l){
	
	bool  qintercept=0;
	int   npoints = l->GetSize();
	
	
	Float_t   penulx = l->GetXcoord(npoints-2); 
	Float_t   penuly = l->GetYcoord(npoints-2);
	Float_t   penulz = l->GetZcoord(npoints-2);
	Float_t   lastx = l->GetXcoord(npoints-1);
	Float_t   lasty = l->GetYcoord(npoints-1);
	Float_t   lastz = l->GetZcoord(npoints-1);
	
	std::array<double,3> par_arr = ComputeParameters(penulx,penuly,penulz,lastx,lasty,lastz);
	if(IsRound){
		if(fx1+fr<lastx || lastx<fx1-fr || fy1+fr<lasty || lasty<fy1-fr){
			qintercept = 1; //ratio to be implemented
			}
		}
	else{
		if(fxy == 'x'){
			if((fx1 > 0 && lastx > fx1) || (fx1 < 0 && lastx < fx1)){
				if(!Disappear(l)){
				qintercept = 1;
				double tpar = Compute_tparx(par_arr[0]);
				std::array<double,3> ncoord_arr = ReturnNewXYZ(tpar,par_arr);
				SetNewLastPoint(l,ncoord_arr);
				Reflectx(lastx,lasty,lastz,l);}
			}}
			
		if(fxy == 'y'){
			if((fy1 > 0 && lasty > fy1) || (fy1 < 0 && lasty < fy1)){
				if(!Disappear(l)){
				qintercept = 1;	
				double tpar = Compute_tparx(par_arr[0]);
				std::array<double,3> ncoord_arr = ReturnNewXYZ(tpar,par_arr);
				SetNewLastPoint(l,ncoord_arr);
				Reflecty(lastx,lasty,lastz,l);}
			}}

		}
	return qintercept;
}

