#include <iostream>
#include "PEanalyser.h"
#include "TGraphErrors.h"


PEanalyser::PEanalyser(){};

PEanalyser::PEanalyser(float minz, float maxz, float minev, float maxev,float zspace,int evspace){
	fminz = minz;
	fmaxz = maxz;
	fminev = minev;
	fmaxev = maxev;
	fzspace = zspace;
	fevspace = evspace;
};

PEanalyser::PEanalyser(TH2F *h1, int rpenum, int ppenum, double zdim,int nev){
	vecofsipm.push_back(h1);
        vecofrpenum.push_back(rpenum);
        vecofppenum.push_back(rpenum);
        vecofzdim.push_back(rpenum);

};
	

PEanalyser::~PEanalyser(){
	delete satgraph;
};
		

void PEanalyser::SetGraphCosmetics(){
	satgraph->SetMarkerStyle(20);
	satgraph->SetMarkerColor(kBlue);
	satgraph->GetXaxis()->SetTitle("Distance [mm]");
	satgraph->GetYaxis()->SetTitle("Saturation ratio [PE_p-PE_r]/N_#gamma");
	satgraph->SetTitle("Photons lost to saturation");

	PCEgraph->SetMarkerStyle(20);
	PCEgraph->SetMarkerColor(kBlue);
	PCEgraph->GetXaxis()->SetTitle("Distance [mm]");
	PCEgraph->GetYaxis()->SetTitle("Average number of photons for one PE");
	PCEgraph->SetTitle("Photon conversion as a function of distance");

	hzevsat->SetStats(0);
	hzevPCE->SetStats(0);

	hzevsat->GetXaxis()->SetTitle("z distance");
	hzevsat->GetYaxis()->SetTitle("N_#gamma");
	hzevPCE->GetXaxis()->SetTitle("z distance");
	hzevPCE->GetYaxis()->SetTitle("N_#gamma");
}


void PEanalyser::AddSet(TH2F *h1,int rpenum, int ppenum, double zdim, int nevent){
	vecofsipm.push_back(h1);
	vecofrpenum.push_back(rpenum);
	vecofppenum.push_back(ppenum);
	vecofzdim.push_back(zdim);
	vecofnev.push_back(nevent);
};

void PEanalyser::Fillhzev(float zdist, int nev, float satratio, float PCE){
	hzevsat->SetBinContent(zdist/fzspace,nev/fevspace,satratio);
	hzevPCE->SetBinContent(zdist/fzspace,nev/fevspace,PCE);
}

void PEanalyser::ComputeSaturation(){
	int looplength= vecofrpenum.size();
	float zdists[looplength];
	float satratio[looplength];
	float photoconratio[looplength];
	hzevsat = new TH2F("hzevsat","Photon ratio lost to saturation [#frac{N_{ppe}-N_{rpe}}{N_#gamma}] map;z distance;event #",int((fmaxz-fminz)/fzspace)+1,fminz,fmaxz,int((fmaxev-fminev)/fevspace)+1,fminev,fmaxev);
	hzevPCE = new TH2F("hzevPCE","Photon conversion efficiency [N_{#gamma}/N_{pe}] map;z distance;event #",int((fmaxz-fminz)/fzspace)+1,fminz,fmaxz,int((fmaxev-fminev)/fevspace)+1,fminev,fmaxev);
	for(int i=0;i< looplength;i++){
		zdists[i]=vecofzdim.at(i);
		satratio[i]=float((vecofppenum.at(i))-(vecofrpenum.at(i)))*float(vecofnev.at(i));
		photoconratio[i]=float(vecofrpenum.at(i))/vecofnev.at(i);
		Fillhzev(vecofzdim.at(i),vecofnev.at(i),satratio[i],photoconratio[i]);
	}
	
	satgraph = new TGraphErrors(looplength,zdists,satratio);
	PCEgraph = new TGraphErrors(looplength,zdists,photoconratio);
	/*
	ppegraphev = new TGraphErrors(looplength,zdists,satratio);
	rpegraphev = new TGraphErrors(looplength,zdists,satratio);
	ppegraphz = new TGraphErrors(looplength,zdists,satratio);
	ppegraphz = new TGraphErrors(looplength,zdists,satratio);
	*/

	SetGraphCosmetics();
};
