#include"photon.h"
#include"TPolyLine3D.h"
#include<vector>
#include<iostream>
photon::photon(){
	std::cout<<"Default constructor called"<<std::endl;
	double x[2] ={0,1};
	double y[2] ={0,1};
	double z[2] ={0,1};
	l=new TPolyLine3D(2,x,y,z);
	
	for(int i=0;i<2;i++){
		xcoord.push_back(x[i]);
		ycoord.push_back(y[i]);
		zcoord.push_back(z[i]);
		ncoord++;
	}

}

photon::photon(double *x,double *y,double *z){
	if(sizeof(x) != sizeof (y) || sizeof(y) != sizeof(z) || sizeof(z) != sizeof (x)){
	std::cout << "Wrong size of input array !" << std::endl;
	}
	else{
		int s = 2;//int((sizeof(x)/sizeof(*x)));
		l=new TPolyLine3D(s,x,y,z);

		for(int i=0;i<s;i++){
			xcoord.push_back(x[i]);
			ycoord.push_back(y[i]);
			zcoord.push_back(z[i]);
			ncoord++;
		}

		}
	}

	photon::~photon(){
		delete l;	
	}



	void photon::SetNewLastPoint(double x, double y, double z){
		l->SetPoint(ncoord,x,y,z);
	}

	void photon::SetNextPoint(double x, double y, double z){
		l->SetNextPoint(x,y,z);
		xcoord.push_back(x);
		ycoord.push_back(y);
		zcoord.push_back(z);
		ncoord++;
	}

std::array<float,3> photon::ReturnEquation(){
    std::array<float,3> N_minus1_point = {xcoord[this->GetSize()],ycoord[this->GetSize()],zcoord[this->GetSize()]};
    std::array<float,3> Npoint = {xcoord.back(),ycoord.back(),zcoord.back()};
    Npoint[0] = Npoint[0]-N_minus1_point[0];
    Npoint[1] = Npoint[1]-N_minus1_point[1];
    Npoint[2] = Npoint[2]-N_minus1_point[2];

    return Npoint;
}

float photon::ReturnTheta_Plane_Line(float a, float b, float c, float vx, float vy, float vz){
    return TMath::ASin((a*vx+b*vy+c*vz)/(TMath::Sqrt(a*a+b*b+c*c)*TMath::Sqrt(vx*vx+vy*vy+vz*vz)));
}


float photon::ReturnAnglefromPlane(float a,float b,float c,float d){ //Angle of photon compared to plane input plane equation parameters see RefractiveVolume.h
    std::array<float,3> xyz_photon_trajec = ReturnEquation();
    float theta = ReturnTheta_Plane_Line(a,b,c,xyz_photon_trajec[0],xyz_photon_trajec[1],xyz_photon_trajec[2]);
    return theta;
    }
