#ifndef PHOTON_H
#define PHOTON_H


#include"TPolyLine3D.h"
#include<vector>
#include<array>
#include"TMath.h"

class photon{
	public:
		photon();
		photon(double *x,double *y,double *z);
		virtual ~photon();
		void SetNewLastPoint(double x, double y, double z);
		void SetNextPoint(double x, double y, double z);
		int  GetSize(){return xcoord.size();};
		double GetXcoord(int index){return xcoord.at(index);};
		double GetYcoord(int index){return ycoord.at(index);};
		double GetZcoord(int index){return zcoord.at(index);};
		void Draw(Option_t *option=""){l->Draw(option);};
		void SetLineColor(int color){l->SetLineColor(color);};
		bool GetIsDisappear(){return IsDisappear;};
	    float ReturnAnglefromPlane(float a,float b,float c,float d);
    std::array<float,3> ReturnEquation();
    private:
        float ReturnTheta_Plane_Line(float a, float b, float c, float vx, float vy, float vz);
		void SetIsDisappear(){IsDisappear=1;};
		TPolyLine3D *l;
		std::vector<double> xcoord;
		std::vector<double> ycoord;
		std::vector<double> zcoord;
		int ncoord=0; //number of coordinates
		bool IsDisappear=0;

        friend class RefractiveVolume;
        friend class wall;
};

#endif
