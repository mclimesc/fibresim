#include <iostream>
#include <vector>
#include <time.h>
#include "TCanvas.h"
#include "TLine.h"
#include "TVector3.h"
#include "TLatex.h"
#include "TGraph2D.h"
#include "TF1.h"
#include "TH2F.h"
#include "TMath.h"
#include "TRandom3.h"
#include "TPolyLine3D.h"
#include "wall.h"
#include "TEllipse.h"
#include "TStyle.h"

struct container{
	TH2F *hvisu;
	int ppe;
	int rpe;
	double zdim;

	~container(){
		delete hvisu;
	}
};


float roundoff(float value, unsigned char prec){
  float pow_10 = pow(10.0f, (float)prec);
  return round(value * pow_10) / pow_10;
};




std::vector<int> ComputeBinTotal(double cutoffangle=0){
	std::vector<double> vh = {0.0015,0.0045,0.0085,0.0125,0.0165,0.0195,0.0235,0.0265,0.0295,0.0325,0.0365,0.0385,0.0415,0.0435,0.0445,0.0465,0.0475,0.0485,0.0475,0.0455,0.0452,0.045,0.044,0.0435,0.039,0.0325,0.0245,0.02,0.0165,0.0145,0.0125,0.0095,0.00875,0.00765,0.0065,0.0056,0.0049,0.0042,0.0037,0.0032,0.0026,0.0021,0.0016,0.0011,0.0005};
	int total= 0;
	std::vector<int> ang_cdf={};
	if(cutoffangle){    
	for(auto &a:vh){
		if(cutoffangle > 0){
        		cutoffangle=cutoffangle -2;
			a=100000*a;
        	        total+= a;
			ang_cdf.push_back(total);
		}
            }
	}
	else{
	    for(auto &a:vh){
			a=100000*a;
        	        total+= a;
			ang_cdf.push_back(total);
		}
            }
	return ang_cdf;
}

std::vector<double> GenVecofA(double cutoffangle=0){
	std::vector<double> A;
	if(cutoffangle){
		for(int i=2;i<=int(cutoffangle);i=i+2){
			A.push_back(i);
		}
	}
	else{
		for(int i=2;i<=90;i=i+2){
			A.push_back(i);
		}
	}
	return A;
	
}

double GenerateAngle(TRandom3 *rng,std::vector<double> vecofA,std::vector<int> ang_cdf){
	int total=ang_cdf[ang_cdf.size()-1];
	double gennum=  (rng->Uniform(0,total));
	
	auto low = std::lower_bound(ang_cdf.begin(),ang_cdf.end(),gennum);
	
	double genjitter = -2*(rng->Rndm());
	
	return (vecofA[(low-ang_cdf.begin())]+genjitter);

}


std::array<int,2> PredPE(TH2F *hvisu,TRandom3 *rng){
	int pe(0),bincontent(0),ppe(0);
	int nbinsx=hvisu->GetNbinsX();
	int nbinsy=hvisu->GetNbinsY();
	bool fired =0;
	float pixelefficiency = 0.4;
	for(int x=0;x<nbinsx;x++){
		for(int y=0;y<nbinsy;y++){
			fired=0;
			bincontent=hvisu->GetBinContent(x,y);
			if(bincontent){
				ppe++;
				for(int ex =0;ex<bincontent;ex++){
					if(fired==0 && rng->Rndm()<pixelefficiency){	
						pe++;
						fired =1;
					}
				}
			}
		}
	}
	std::array<int,2> pes = {pe,ppe};
	return  pes;
}


std::array<double,3> GetSlopes(double *polyx, double *polyy, double *polyz){
	std::array<double,3> poly = {(polyx[0]+polyx[1])/2,(polyy[0]+polyy[1])/2,(polyz[0]+polyz[1])/2};
	return poly; 
 }

void FillVisualize(TH2F *h1,double xend, double yend){
	h1->Fill(xend,yend);

}

void LoopforCheck(std::vector<wall*> &vecofwalls,photon *line, bool &refl, int l0, int l1){
	for(int nwall = l0;nwall<l1;nwall++){
                                          refl=vecofwalls.at(nwall)->IsIntercept(line);
	}
}

void CheckforReflx(bool &refl, photon *line, std::vector<wall*> vecofwalls, int debug, int nev, int ev, int wallno){
		while(refl == 1){
				LoopforCheck(vecofwalls, line,refl,0,wallno/2);
		}
}

void CheckforRefly(bool &refl, photon* line, std::vector<wall*> vecofwalls, int debug, int nev, int ev,int wallno){
		while(refl == 1){
				LoopforCheck(vecofwalls, line,refl,wallno/2,wallno);
		}
}

Double_t ToRad(Double_t in){
	return in*(TMath::Pi())/180.;
}

TEllipse *ProduceFibreshape(double x1,double y1, double r1, double r2){
	return new TEllipse(x1,y1,r1,r2);
}

/*TH2F *diffusorsim(double zdim=3,double cutoffangle=90, int nev = 10000){
	diffusorsim(zdim,cutoffangle,nev);
}*/

container diffusorsim(double zdim=3,double cutoffangle=90, int nev = 10000, TRandom3 *rng=new TRandom3(),int nofibre =2,double sipmside=6,int n_pixels=14400,int debug=0){
//	gStyle->SetPalette(kSunset);
	TString version("0.6b_imperfectwalls");
	time_t start,end;
	char mode = 'r'; //r for real, s for simplified
    if(debug){
        std::cout << "debug is " << debug << std::endl;	
    }
        //	Double_t zdim = 3; //in mm

	Double_t interfibdist; 
	Double_t fibrad1;
	Double_t fibrad2;
    if(nofibre==2){
	    interfibdist = 2.2; //distance between the two fibers
	    fibrad1 = 0.5;
	    fibrad2 = 0.5;
    }	
   
    if(nofibre==1){
	    fibrad1 = 0.6;
    }
    
   TH1F *hangledebug;     
   TH1F *hzlocationdebug; 

    int hsideno = int(TMath::Sqrt(n_pixels))*2;
	TH2F *hvisu = new TH2F(Form("hvisu%f_%d",roundoff(zdim,2),nev),Form("%d Photon repartition on receiving plane, distance = %.2f mm, opening angle up to 90#circ;x;y",nev,zdim),hsideno,-sipmside,sipmside,hsideno,-sipmside,sipmside);
		
	TH2F *inihvisu;
	TH2F *genhvisu;
	if(debug==2){
		inihvisu = new TH2F(Form("inihvisu%f_%d",roundoff(zdim,2),nev),"Photon repartition on the fibres",120,-6,6,120,-6,6);
		genhvisu = new TH2F(Form("genhvisu%f_%d",roundoff(zdim,2),nev),"Photon repartition after initial generation",120,-6,6,120,-6,6);
		inihvisu->SetStats(0);
		genhvisu->SetStats(0);
        hangledebug=new TH1F(Form("hangledebug%d",int(zdim)),"angle debug",90,0,90);
        hzlocationdebug=new TH1F(Form("hzlocationdebug_%d",int(zdim)),"zlocation debug",90,0,90);  
	}
	//TH1F *hz = new TH1F("hz","final Z positions",10,-3,3);
	hvisu->SetStats(0);
	
	std::array<double,3> startpoint = {0,0,0};
	std::array<double,3> genpoint  = {0,0,zdim};
    
    double origincenter1[3];
    double origincenter2[3];
    
    TEllipse *fibre1shape;
    TEllipse *fibre2shape; 
    
    if(nofibre==2){	
	    origincenter1[0] = interfibdist/2;
	    origincenter1[1] = interfibdist/2;
	    origincenter1[2] = 0; 
	    origincenter2[0] = -interfibdist/2;
	    origincenter2[1] = -interfibdist/2;
	    origincenter2[2] = 0; 

	    fibre1shape = ProduceFibreshape(origincenter1[0],origincenter1[1],fibrad1,fibrad1);
	    fibre2shape = ProduceFibreshape(origincenter2[0],origincenter2[1],fibrad2,fibrad2);
        	
	    fibre1shape->SetFillColorAlpha(kWhite,0.5);
	    fibre2shape->SetFillColorAlpha(kWhite,0.5);
    }
    else{
        origincenter1[0]=0;
        origincenter1[1]=0;
        origincenter1[2]=0;
	    fibre1shape = ProduceFibreshape(origincenter1[0],origincenter1[1],fibrad1,fibrad1);
	    fibre1shape->SetFillColorAlpha(kWhite,0.5);
    }
    std::vector<photon*> vecoflines;
	std::vector<std::array<double,3>> vecofOr;
	

	double polyx[2]; 
	double polyy[2]; 
	double polyz[2]; 
	unsigned fibre;
	std::vector<double> vecofA;
        std::vector<int> ang_cdf;
	
	if(mode == 'r'){
		vecofA = GenVecofA();
		ang_cdf = ComputeBinTotal(cutoffangle);
	}

	if(debug==4){
		time(&start);
	}
    double opening_angle;
	for(int ev =0;ev < nev; ev++){
		if(!(ev%100000)){
			std::cout<< "Event #" << ev << " of " << nev << " dist " << zdim << std::endl;
			if(debug ==4){
				time(&end);
				std::cout << double(end - start) << std::endl;
			}
		}
		if(mode == 'r') opening_angle = ToRad(GenerateAngle(rng,vecofA,ang_cdf));		
        if(nofibre==2){
        fibre = rng->Integer(2);}
        else if(nofibre==1){fibre=0;}
		double angle = rng->Rndm()*2*TMath::Pi();
		double anglegen = rng->Rndm()*2*TMath::Pi();
		double radius(999999999);
		if(fibre==0){
//			while(radius > fibrad1){
			radius = fibrad1*TMath::Sqrt(rng->Rndm());
//			}
			startpoint[0] = origincenter1[0]+ radius*TMath::Cos(angle);
			startpoint[1] = origincenter1[1]+ radius*TMath::Sin(angle);
			if(debug==2){
				inihvisu->Fill(startpoint[0],startpoint[1]);
			}
			
			double posx(1000000),posy(1000000),posz(1000000);
			double radgen;
			if(mode == 's')	radgen = TMath::Sqrt(rng->Uniform(0,1))*(fibrad2 + TMath::Tan(opening_angle)*zdim);
			if(mode == 'r') radgen = TMath::Sqrt(rng->Uniform(0,1))*(fibrad2 + TMath::Tan(opening_angle)*zdim);
            

            posx = origincenter1[0]+radgen*TMath::Cos(anglegen);
            genpoint[0] = posx;
            posy = origincenter1[1]+radgen*TMath::Sin(anglegen);
            
            if(debug == 6){
                hangledebug->Fill(opening_angle);
                hzlocationdebug->Fill(TMath::Sqrt(posx*posx+posy*posy));
            }
            genpoint[1] = posy;
			genpoint[2] = zdim;
			if(debug==2){	
				genhvisu->Fill(genpoint[0],genpoint[1]);
			}
			vecofOr.push_back(startpoint);
			polyx[0] = startpoint[0];
			polyx[1] = genpoint[0];
			polyy[0] = startpoint[1];
			polyy[1] = genpoint[1];
			polyz[0] = 1;
			polyz[1] = zdim;
			vecoflines.push_back(new photon(polyx,polyy,polyz));
			vecoflines.back()->SetLineColor(kGreen+3);
		}

		else{
              // 		while(radius > fibrad2){
               		        radius = fibrad2*TMath::Sqrt(rng->Rndm());
		//}
               		startpoint[0] = origincenter2[0]+ radius*TMath::Cos(angle);
               		startpoint[1] = origincenter2[1]+ radius*TMath::Sin(angle);
	       		if(debug==2){	
	       			inihvisu->Fill(startpoint[0],startpoint[1]);
	       		}
               		double posx(1000000),posy(1000000),posz(1000000);
	       			double radgen;	
	       			if(mode == 's') radgen = TMath::Sqrt(rng->Uniform(0,1))*(fibrad2 + TMath::Tan(opening_angle)*zdim);
	       			if(mode == 'r') radgen = TMath::Sqrt(rng->Uniform(0,1))*(fibrad2 + TMath::Tan(opening_angle)*zdim);
        	posx = origincenter2[0]+radgen*TMath::Cos(anglegen);
        	genpoint[0] = posx;
        	posy = origincenter2[1]+radgen*TMath::Sin(anglegen);
        	genpoint[1] = posy;
               	genpoint[2] = zdim;
               	vecofOr.push_back(startpoint);
                polyx[0] = startpoint[0];
                polyx[1] = genpoint[0];
                polyy[0] = startpoint[1];
                polyy[1] = genpoint[1];
                polyz[0] = 0;
                polyz[1] = zdim;
                polyx[0] = startpoint[0];
	       	if(debug==2){
	       			genhvisu->Fill(genpoint[0],genpoint[1]);
               			vecoflines.back()->SetLineColor(kGreen+3);}
		}
                vecoflines.push_back(new photon(polyx,polyy,polyz));
//		auto polies = GetSlopes(polyx,polyy,polyz);
		std::vector<wall*> vecofwalls;
		vecofwalls.push_back(new wall(-sipmside/2,-sipmside/2,-sipmside/2,sipmside/2,'x',rng));
		vecofwalls.push_back(new wall(sipmside/2,sipmside/2,sipmside/2,-sipmside/2,'x',rng));
		vecofwalls.push_back(new wall(-sipmside/2,sipmside/2,sipmside/2,sipmside/2,'y',rng));
		vecofwalls.push_back(new wall(-sipmside/2,sipmside/2,-sipmside/2,-sipmside/2,'y',rng));
		bool refl = 1;
		CheckforReflx(refl,vecoflines.back(),vecofwalls,debug,nev,ev,vecofwalls.size());
		refl = 1;
		CheckforRefly(refl,vecoflines.back(),vecofwalls,debug,nev,ev,vecofwalls.size());
		vecofwalls.clear();
			if(debug==3){
				for(int i=0;i< vecoflines.size();i++){
				if(vecoflines.at(i)->GetXcoord(vecoflines.at(i)->GetSize()-1) < -3){
					std::cout << "failx " << vecoflines.at(i)->GetXcoord(vecoflines.at(i)->GetSize()-1) << std::endl;
					std::cout << "faily " << vecoflines.at(i)->GetYcoord(vecoflines.at(i)->GetSize()-1) << std::endl;
				}}
			}

		if(!(vecoflines.back()->GetIsDisappear())){
			double fillx = vecoflines.back()->GetXcoord(vecoflines.back()->GetSize()-1);
			double filly = vecoflines.back()->GetYcoord(vecoflines.back()->GetSize()-1);
			FillVisualize(hvisu,fillx,filly);
		}
	}
    int nar = vecofOr.size();


	TLatex l;
   	l.SetTextSize(0.025);
   	l.SetTextAngle(30.);
    if(debug==1){
    	TCanvas *c1 = new TCanvas("c1","c1",600,600);
    	TGraph2D *phg = new TGraph2D();
    		phg->SetMarkerStyle(20);
    		phg->SetTitle(Form("Simulated Fibre output for %d photons",nev));
    		phg->Draw("pcol");
    		for(int i=0;i< nar;i++){
    			phg->SetPoint(i,vecofOr[i][0],vecofOr[i][1],1);
    			//phg->SetPoint(i,1,1,1);
    			vecoflines.at(i)->Draw("SAME");
    		}
    		phg->AddPoint(1,1,2);
    		phg->AddPoint(0,0,3);
    		phg->GetXaxis()->SetTitle("x coordinates");
    		phg->GetYaxis()->SetTitle("y coordinates");
    		phg->GetZaxis()->SetTitle("z coordinates");
      	      	l.DrawLatex(-0.85,-0.85,"Version "+version);
      	      	c1->SaveAs("simulatedoutput"+version+".pdf");
    
    	    TCanvas *cz = new TCanvas("cz","cz",600,600);
    	    cz->cd();
    	//	for(int ki=0;ki<vecoflines.size();ki++){
    //		hz->Fill(vecoflines.at(ki)->GetZcoord(vecoflines.at(ki)->GetSize()-1));
    //	}
    //	hz->Draw();
    	
    	    cz->SetLogy();
    	
    	    cz->SaveAs("zcheck.pdf");
    	}
	
	std::array<int,2> pes = PredPE(hvisu,rng);
	if(debug == 5){
        TCanvas *c2 = new TCanvas("c2","c2",600,600);
	    hvisu->SetStats(0);
	    hvisu->Draw("COLZ");
   	    l.SetTextAngle(0.);
	    l.DrawLatex(-6,-7,"Version "+version);
	    l.DrawLatex(-6,6.2,Form("PE number %d, with perfect pixels %d",pes[0],pes[1]));
	    
        fibre1shape->Draw("F SAME");
	    if(nofibre==2){
            fibre2shape->Draw("F SAME");}
	    c2->SaveAs(Form("hvisu_d%f_e%d.pdf",zdim,nev));
	}
	if(debug==2){
		TCanvas *c3 = new TCanvas(Form("c3_%d",int(zdim)),"c3",600,600);
		inihvisu->Draw("COLZ");
		fibre1shape->Draw("SAME");
		if(nofibre==2){
    		fibre2shape->Draw("SAME");}
		c3->SaveAs("inihvisu.pdf");
        delete c3;
		TCanvas *c4 = new TCanvas(Form("c4_%d",int(zdim)),"c4",600,600);
		genhvisu->Draw("COLZ");
		fibre1shape->Draw("SAME");
		if(nofibre==2){
    		fibre2shape->Draw("SAME");}
		c4->SaveAs("genhvisu.pdf");
        delete c4;

		TCanvas *cangledebug = new TCanvas(Form("cangledebug_%d",int(zdim)),"cangledebug",800,600);
        hangledebug->Draw();
		TCanvas *czlocdebug = new TCanvas(Form("czlocdebug_%d",int(zdim)),"czlocdebug",800,600);
        hzlocationdebug->Draw();
        
        cangledebug->SaveAs(Form("angledebug_%d.pdf",int(zdim)));
        czlocdebug->SaveAs(Form("zlocdebug_%d.pdf",int(zdim)));
	}
	container contain;
	contain.hvisu = hvisu;
	contain.ppe =pes[1];
	contain.rpe =pes[0];
	contain.zdim =zdim;
	if(debug==2){
		delete inihvisu;
		delete genhvisu;}
	delete fibre1shape;
    if(nofibre==2){
    delete fibre2shape;}
	for(photon *pointer : vecoflines){
		delete pointer;
	}
	vecoflines.clear();
	return contain;
}
