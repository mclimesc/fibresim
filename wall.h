#ifndef WALL_H
#define WALL_H


#include <iostream>
#include "TPolyLine3D.h"
#include <array>
#include <vector>
#include "photon.h"
#include "TRandom3.h"
class wall{
	
	public:
		wall();
		wall(double x1,double x2,double y1,double y2, char xy,TRandom3* rng); //constructor for straight wall, type for x or y wall, walls parallel with the axis only
		wall(double x1,double y1,double r,double ratio,bool round = 0); //constructor for round wall, ratio starts at theta = 0
		virtual ~wall();
		bool GetIsRound() {return IsRound;};
		bool IsIntercept(photon *l);
		void Draw();
		double GetICoord(){double ret; if(fxy == 'x') ret = fx1; if(fxy == 'x') ret = fy1; return ret;};
	private:
		std::array<double,3> ComputeParameters(double x1,double y1,double z1,double x2,double y2,double z2);
		std::array<double,3> ReturnNewXYZ(double tpar, std::array<double,3> par_arr);
		double Compute_tparx(double xpar);
		double Compute_tpary(double ypar);
		void SetNewLastPoint(photon *l, std::array<double,3> ncoord_arr);
		void Reflectx(double lastx, double lasty, double lastz,photon *l);
		void Reflecty(double lastx, double lasty, double lastz,photon *l);
		bool Disappear(photon *l);
		TRandom3 *mrng;
		double fx1,fx2,fy1,fy2,fr;
		double freflectivity = 0.99;
		bool IsRound=0;
		char fxy;	
		int debug= 1;
};

#endif
