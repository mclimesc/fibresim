#include <iostream>
#include "diffusorsim.cxx"
#include "PEanalyser.h"
#include "TRandom3.h"
#include "TFile.h"
#include "TTree.h"
#include <fstream>
container diffusorsim(double zdim,double cutoffangle, int nev);

void Initialize(int &minev, int &maxev,int &evstep,float &minzdim,float &maxzdim,float &zdimstep,bool  &runsimulation,float &anglecutoff,int &nofibre,float &sipmside,int &n_pixels,int &debug){
	std::ifstream initfile("initialize.ini");
	TString name;
	float value;
	while(initfile >> name >> value){
			 if(name=="minev")
			    minev = int(value); 
			 if(name=="maxev")
			    maxev = int(value);
			 if(name=="evstep")
			    evstep = int(value);
			 if(name=="minzdim")
			    minzdim = value; 
			 if(name=="maxzdim")
			    maxzdim = value;
			 if(name=="zdimstep")
			    zdimstep = value;
			 if(name=="runsimulation")
			    runsimulation = bool(value);
			 if(name=="anglecutoff")
			    anglecutoff = value;
			 if(name=="nofibre")
			    nofibre = value;
			 if(name=="sipmside")
			    sipmside = value;
			 if(name=="n_pixels")
			    n_pixels = value;
			 if(name=="debug")
			    debug = int(value);
		}
}


int main(){
	int minev =1000;
	int maxev =10000;
	int evstep = 1000;
    
	float minzdim =3;
	float maxzdim =3;
	float zdimstep = 1;
	float anglecutoff=90;
	bool  runsimulation=0;
	int nofibre=2;
	float sipmside=6;
    int n_pixels=14400;
    int debug =0;	
	bool _analyse_ =0;
    Initialize(minev,maxev,evstep,minzdim,maxzdim,zdimstep,runsimulation,anglecutoff,nofibre,sipmside,n_pixels,debug);
	std::cout << n_pixels << std::endl;
	

	PEanalyser *aly = new PEanalyser(minzdim,maxzdim,minev,maxev,zdimstep,evstep); //float minz, float maxz, float minev, float maxev,float zspace,int evspace
	TRandom3 *rng = new TRandom3();
	TFile *f1;
	TTree *t1;
	TH2F *hvisu;
	int rpe,ppe;
	float zzdim;
	int ev;
	if(runsimulation){
		f1 = new TFile("simoutput.root","RECREATE");
		t1 = new TTree("sim_cont","Simulation output: hvisu output histogram, rpe and ppe real and perfect pixel PE counts, zdim, distance from fibres to SiPM");
		t1->Branch("hvisu",&hvisu);
		t1->Branch("rpe",&rpe);
		t1->Branch("ppe",&ppe);
		t1->Branch("zdim",&zzdim);
		t1->Branch("nev",&ev);
	}
	else{
		f1 = new TFile("simoutput.root","READ");
		f1->GetObject("sim_cont",t1);
		std::cout << "before" << std::endl;
		t1->SetBranchAddress("hvisu",&hvisu);
		std::cout << "after" << std::endl;
		t1->SetBranchAddress("rpe",&rpe);
		t1->SetBranchAddress("ppe",&ppe);
		t1->SetBranchAddress("zdim",&zzdim);
		t1->SetBranchAddress("nev",&ev);
	}

	for(float zdim=minzdim;zdim<=maxzdim;zdim=zdim+zdimstep){
		for(int nev=minev;nev<=maxev;nev=nev+evstep){
			ev=nev;
			zzdim=zdim;

			if(runsimulation){
				container contain = diffusorsim(zdim,90,nev,rng,nofibre,sipmside,n_pixels,debug);
                hvisu = contain.hvisu;
				rpe = contain.rpe;
				ppe=contain.ppe;

				zdim=contain.zdim;
				t1->Fill();
				aly->AddSet(hvisu,rpe,ppe,zdim,nev);
			}
		}
	}
	if(!runsimulation){
		int ele = t1->GetEntries();
		for(int tev =0; tev< ele ;tev++){
			t1->GetEntry(ev);
			std::cout << rpe << " " << ppe << std::endl;
			aly->AddSet(hvisu,rpe,ppe,zzdim,ev);
		}
	}
    if(_analyse_){
	    aly->ComputeSaturation();
	    TGraphErrors *g1 = aly->GetSatGraph();
	    TGraphErrors *g2 = aly->GetPCEGraph();
	    TH2F *h1 = aly->Gethzevsat();
	    TH2F *h2 = aly->GethzevPCE();

	    TCanvas *c1 = new TCanvas("c1","c1",900,600);
	    g1->Draw();
	    c1->SaveAs("satgraph.pdf");
	    TCanvas *c2 = new TCanvas("c2","c2",900,600);
	    g2->Draw();
	    c2->SaveAs("PCEgraph.pdf");
	    
	    TCanvas *c3 = new TCanvas("c3","c3",900,600);
	    h1->Draw("COLZ");
	    c3->SaveAs("hsat.pdf");
	    TCanvas *c4 = new TCanvas("c4","c4",900,600);
	    h2->Draw("COLZ");
	    c4->SaveAs("hPCE.pdf");}
	if(runsimulation){
	f1->WriteTObject(t1);}
	f1->Close();

}
