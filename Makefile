Target  = run.exe all:$(Target) # To include headers, use -I$PATHTOFHEADERFOLDER # To include library, use -L$PATHTOFLIBFOLDER, and use -l$NAMEOFLIBRARY (*.so file, without the prefix lib and without the extension .so) 

run.exe : wall.o photon.o main.o PEanalyser.o
	g++ -std=c++1y main.o photon.o wall.o PEanalyser.o -o run.exe `root-config --cflags --glibs` -lTreePlayer -lTMVA 
main.o : main.cpp 
	g++ -std=c++1y -o main.o -c main.cpp `root-config --cflags --glibs` -lTreePlayer 
wall.o: wall.cxx wall.h	
	g++ -std=c++1y -o wall.o -c wall.cxx `root-config --cflags --glibs` -lTreePlayer 
photon.o: photon.cxx photon.h	
	g++ -std=c++1y -o photon.o -c photon.cxx `root-config --cflags --glibs` -lTreePlayer 
PEanalyser.o : PEanalyser.cxx PEanalyser.h
	g++ -std=c++1y -o PEanalyser.o -c PEanalyser.cxx `root-config --cflags --glibs` -lTreePlayer 
INPUTS.o : INPUTS.h
	g++ -std=c++1y -o INPUTS.o  `root-config --cflags --glibs` -lTreePlayer 
clean :
	rm -f *.o
	rm -f run.exe
