#ifndef PEANALYSER_H
#define PEANALYSER_H


#include <iostream>
#include <vector>
#include "TH2F.h"
#include "TGraphErrors.h"
#include "TCanvas.h"


class PEanalyser{
	
	public:
		PEanalyser();
		PEanalyser(float minz, float maxz, float minev, float maxev, float zspace, int evspace);
		PEanalyser(TH2F *h1, int rpenum, int ppenum, double zdim, int nphoton);
		virtual ~PEanalyser();
		void AddSet(TH2F *h1, int rpenum, int ppenum, double zdim,int nphoton);
		void ComputeSaturation();
		TGraphErrors *GetSatGraph(){return satgraph;};
		TGraphErrors *GetPCEGraph(){return PCEgraph;};
		TH2F *Gethzevsat(){return hzevsat;};
		TH2F *GethzevPCE(){return hzevPCE;};
	private:
		void SetGraphCosmetics();
		void Fillhzev(float zdist, int nev, float satratio, float PCE);
		int fevspace;
		float fminz,fmaxz,fminev,fmaxev,fzspace;

		std::vector<TH2F *> vecofsipm;
		std::vector<int> vecofrpenum;
		std::vector<int> vecofppenum;
		std::vector<int> vecofnev;
		std::vector<double> vecofzdim;
		TGraphErrors *satgraph;
		TGraphErrors *PCEgraph;
		TGraphErrors *ppegraphev;
		TGraphErrors *rpegraphev;
		TGraphErrors *ppegraphz;
		TGraphErrors *rpegraphz;
		TH2F *hzevsat;
		TH2F *hzevPCE;
};

#endif
