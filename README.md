##############Fibre on surface diffusion simulation#########################

Current version: 0.6


###To run the simulation:###
git clone ssh://git@gitlab.cern.ch:7999/mclimesc/fibresim.git
cd fibresim
make
./run.exe

Results are stored in an output .root file which can be read afterwards. Analysis has passed initial trials. Debug information for individual runs can be produced in one (debug == 0) or three (debug==3) histograms.

Settings are set in initialize.ini. No recompilation is needed in between setting changes. See initialization variable meanings below 

make clean is supported for recompilation needs.

An Analyser is provided, it should be regarded as in alpha in version v0.6b

###How does it work?


1) Parameters (distance from plane surface, maximum allowed angle, fibre size) are set in main.cpp, reflectivity is set directly in the wall.h class header.
2) Reflective walls are created (!in version 0.5 only perfectly reflective line walls are available, imperfect walls will be made available in version 0.6, round walls will be implemented in version 0.7!)
3) Starting point is randomely generated on the fibres
4) End point is generated within the set parameters (real distribution by default, uniform distribution can be set but require intervention in the code)
5) Reflections by walls are examined until the photon traverses the final distance between fibres and sensitive surface
6) Main output is a histogram of final photon positions
7) Debug outputs are a graph of trajectories (!very crowded for high occupancy!) and a final Z position histogram.
8) PE numbers are computed assuming each pixel can only fire once and has a 40% chance of firing for each photon hitting it (as is the case for Hamatasu S13360-6050PE for green light). As such, a pixel hit by two photons has a 64% chance of firing. A distinction is made between Perfect PhotonElectrons (ppe) and Real PhotoElectrons (rpe). The former assumes 100% efficiency from pixels.
9) By default the SiPM is set to be centered in (0,0,0), facing towards (0,0,1)

###Upcoming features

0.6: imperfect reflectivity
0.7: round walls


###Initialiation variables:

minev 			: minimum number of photons generated in one run
maxev 			: maximum number of photons generated in one run
evstep 			: event number step size between runs
minzdim 		: minimum distance between fibre and surface in one run, distances of 0 are not recommended
maxzdim 		: maximum distance between fibre and surface in one run
zdimstep 		: distance step size
runsimulation	: whether to run the simulation and produce a tree or simply analyse the data
anglecutoff		: maximum allowed angle (in degrees), few photons produced above 75 degrees but value is not sensitive as far as performance goes. 90 is the highest physical angle.
nofibre         : Number of fibres (no more than 2!)
sipmside        : The size (in mm) of the SiPM side
n_pixels        : Number of pixels
debug			: whether a debug feature should be used. Most useful is likely debug == 5 to provide a SiPM image at each distance for each set of events saved as hvisu_d[distance]_e[event number].pdf 
